package notificacion.morazambranoleninjordano.facci.notificacion;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmsReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Toast.makeText(context, "Mensaje recivido", Toast.LENGTH_LONG).show();

        if (!intent.getAction().equals("android.provider.Telephony.SMS_RECEIVER")){
            return;
        }
        Bundle bundle = intent.getExtras();
        Object[] data = (Object[]) bundle.get("pdus");
        String phone = SmsMessage.createFromPdu((byte[])data[0]).getOriginatingAddress();
        String message = SmsMessage.createFromPdu((byte[])data[1]).getDisplayMessageBody();
        Toast.makeText(context, parseCode(message), Toast.LENGTH_LONG).show();
    }

    //Sacar el numero de un numero
    private String parseCode(String message){
        Pattern p = Pattern.compile("\\b\\d(4)\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()){
            code = m.group(0);
        }
        return code;
    }
}
